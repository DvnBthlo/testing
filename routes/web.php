<?php
// Show all reg. clients
$router->get('/', ['uses' => "ClientsController@list", 'as' => "client.list"]);

// Form registration
$router->get('/registration', ['uses' => "ClientsController@registration", 'as' => "client.reg"]);

// Data from registration form
$router->post('/registration', ['uses' => "ClientsController@store", 'as' => "client.reg.post"]);

