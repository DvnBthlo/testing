<?php
namespace App\Http\Controllers;
/*
* Author: Lukáš Slabý
* Date: 24.11.2018 Prague
* PHPStorm
*/

use DB;
use Illuminate\Http\Request;
use App\Client;
use Validator;

class ClientsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(){
        $clients = Client::all();

        return view("list", ['clients' => $clients]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function registration(){
        return view("registration");
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector|\Illuminate\View\View
     */

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:clients',
            'surname' => 'required|string',
            'birthdate' => 'required|date|before:today|after:1900-01-01'
        ]);
        if($validator->fails()){
            return view("registration", ['errors' => $validator->errors()]);
        }
        $client = new Client;
        $client->name = $request->name;
        $client->surname = $request->surname;
        $client->email = $request->email;
        $client->birthdate = $request->birthdate;
        $client->save();

        return redirect('/');

    }
}
