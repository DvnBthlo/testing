<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $primaryKey = 'PK_idc';
    protected $table = 'clients';
    protected $fillable = [
        'name', 'surname', 'email','birthdate'
    ];
}
