@extends('layouts.app')

@section('body')

    <h1>Registrační stránka</h1>
    @if (isset($errors))
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<form action="{{ route("client.reg.post") }}" method="post"  class="col-sm">
    <div class="form-group">
        <label for="Name">Jméno</label>
        <input type="text" class="form-control" name="name" placeholder="Vaše jméno" required>
    </div>
    <div class="form-group">
        <label for="SurName">Příjmení</label>
        <input type="text" class="form-control" name="surname" placeholder="Vaše příjmení" required>
    </div>
    <div class="form-group">
        <label for="Email">Email</label>
        <input type="email" class="form-control" name="email" placeholder="Vaš email" required>
    </div>
    <div class="form-group">
        <label for="Email">Datum narození</label>
        <input type="date" class="form-control" name="birthdate" placeholder="" required>
    </div>
    <button type="submit" class="btn btn-primary">Odeslat</button>
</form>
@endsection
