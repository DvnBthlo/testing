@extends('layouts.app')

@section('body')
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Jméno</th>
            <th scope="col">Příjmení</th>
            <th scope="col">Email</th>
            <th scope="col">Datum narození</th>
        </tr>
        </thead>
        <tbody>
        @foreach($clients as $client)
            <tr>
                <th scope="row">{{ $client->PK_idc }}</th>
                <td>{{ $client->name }}</td>
                <td>{{ $client->surname }}</td>
                <td>{{ $client->email }}</td>
                <td>{{ \Carbon\Carbon::parse($client->birthdate)->format('d.m.Y') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
